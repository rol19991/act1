﻿using Acme.Biz;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acme.Biz.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void HolaTest()
        {
            //Arrange
            var p1 = new Producto();
            p1.Idproducto = 1;
            p1.NombreProducto = "calamares";
            p1.Descripcion = "bocadillo";

            var expected = "Hola calamares( 1 ): bocadillo";

            //Actual
            var actual = p1.Hola();

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HolaTest1()
        {
            //Arrange
            var p2 = new Producto(2, "caldo", "sopa de fideos");

            var expected = "Hola caldo( 2 ): sopa de fideos";

            //Actual
            var actual = p2.Hola();

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HolaaTest()
        {
            //Arrange
            var p3 = new Producto();
            p3.Idproducto = 3;
            p3.NombreProducto = "tortilla";
            p3.Descripcion = "bocadillo";

            var expected = "hola2tortilla3bocadillo";

            //Actual
            var actual = p3.Holaa();

            //Assert 
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HolaaTest1()
        {
            //Arrange
            var p4 = new Producto(4,"pato","a la naranja");


            var expected = "hola2pato4a la naranja";

            //Actual
            var actual = p4.Holaa();

            //Assert 
            Assert.AreEqual(expected, actual);
        }
    }
}

